import './index.css'
import React from 'react'
import ReactDOM from 'react-dom'
import Board from './Board'
import observe from './Game'
// import BoardSquare from './BoardSquare'
// import renderSquare from './renderSquare'

const root = document.getElementById('root')
observe(knightPosition =>
  ReactDOM.render(<Board knightPosition={knightPosition} />, root),
)


// ReactDOM.render(
//   <Board knightPosition={[7, 4]} />,
//   document.getElementById('root'),
// )
//

//
// ReactDOM.render(
//   <Square black>
//     <Knight />
//   </Square>,
//   document.getElementById('root'),
// )


// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
