import React from 'react'
import Square from './Square'
import Knight from './Knight'
import { canMoveKnight, moveKnight } from './Game'
import BoardSquare from './BoardSquare'

function handleSquareClick(toX, toY) {
 if (canMoveKnight(toX, toY)) {
   moveKnight(toX, toY)
 }
}

  function renderPiece(x, y, [knightX, knightY]) {
    if (x === knightX && y === knightY) {
      return <Knight />
    }
   }

   export default function renderSquare (i, knightPosition, knightX, knightY) {

     const x = i % 8
     const y = Math.floor(i / 8)
     const isKnightHere = x === knightX && y === knightY
     const piece = isKnightHere ? <Knight /> : null
     const black = (x + y) % 2 === 1



    return (
      <div key={i} style={{ width: '12.5%', height: '12.5%' }}
      onClick={(Square) => handleSquareClick(x, y)}>
      <Square black={black}>
      {piece}
      </Square>
      <BoardSquare x={x} y={y}>
      {renderPiece (x, y, knightPosition)}
      </BoardSquare>
      </div>
    )
  }
  
