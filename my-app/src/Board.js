import React from 'react'
import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import renderSquare from './renderSquare'


export default function Board({ knightPosition }) {
    const squares = []
  for (let i = 0; i < 64; i++) {
    squares.push(renderSquare(i, knightPosition))
  }

  return (
    <div style={{
      width: '100%',
      height: '100vh',
      display: 'flex',
      flexWrap: 'wrap',
    }} >
    <DndProvider backend={HTML5Backend}>{squares}</DndProvider>

    </div>
  )
}
